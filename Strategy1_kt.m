function x=Strategy1_kt(x,Param,BestTeam)

n=numel(x.Formation);

for ii=1:n



x.Formation(ii)=x.Formation(ii)+unifrnd(Param.lb,Param.ub);
end

x=simplebound(x,Param);
x.Cost=Param.CostFunction(x.Formation);

end
function Team=Initialized(Team,Leaguesize,Param)
lb=Param.lb;
ub=Param.ub;
nPlayer=Param.nPlayer;
if size(Param.lb,2)>1
    for i=1:Leaguesize
        for j=1:nPlayer
            Team(i).Formation=unifrnd(lb(1,j),ub(1,j),nPlayer,1);
            Team(i).Subsititude=unifrnd(lb(1,j),ub(1,j),nPlayer,1);
        end
        Team(i).Cost=Param.CostFunction(Team(i).Formation);
    end
end
    

for i=1:Leaguesize
    Team(i).Formation=unifrnd(lb,ub,nPlayer,1);
    Team(i).Subsititude=unifrnd(lb,ub,nPlayer,1);
    Team(i).Cost=Param.CostFunction(Team(i).Formation);
end

end
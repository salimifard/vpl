function x=simplebound(x,Param)
lb=Param.lb;
ub=Param.ub;

n=numel(x.Formation);

%  

for i=1:size(x.Formation,1)
         
        % Check if solutions go outside the search spaceand bring them back
        Flag4ub=x.Formation(i,:)>ub;
        Flag4lb=x.Formation(i,:)<lb;
        x.Formation(i,:)=(x.Formation(i,:).*(~(Flag4ub+Flag4lb)))+ub.*Flag4ub+lb.*Flag4lb;
        
        Flag4ub=x.Subsititude(i,:)>ub;
        Flag4lb=x.Subsititude(i,:)<lb;
        x.Subsititude(i,:)=(x.Subsititude(i,:).*(~(Flag4ub+Flag4lb)))+ub.*Flag4ub+lb.*Flag4lb;


end
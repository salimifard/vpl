function Param=CreateParam()
Param.lb=-100;
Param.ub=100;
Param.fallrate=0.15;
Param.TransportationRate=0.36;
Param.par=1;
Param.g=2;



%% General Paramter for algorithm
Param.maxit=100;
Param.Leaguesize=10;
Param.nPlayer=10;
Param.NumberOfFall=ceil(Param.fallrate*Param.Leaguesize);
Param.NumberOfTransportationTeam=ceil(Param.TransportationRate*Param.Leaguesize);

end
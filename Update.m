function [TEAM,BestTeam]=Update(Team,Param,BestTeam)

n1=Param.n1;
n2=Param.n2;
n3=Param.n3;
[~,so]=sort([Team.Cost]);
TEAM=Team;
Team=Team(so);

for i=1:n2
    Team(n1+i).Form.Position=rand*(Team(i).Form.Position);
    Team(n1+i).Form.Knowledge=rand*(Team(i).Form.Knowledge);
    Team(n1+i).Subsititude.Position=rand*(Team(i).Form.Position);
    Team(n1+i).Subsititude.Knowledge=rand*(Team(i).Form.Knowledge);
    Team(n1+i).Cost=MyCost( Team(n1+i).Form.Position,Param.fobj);
end

for i=1:n3
    Team(n1+n2+i).Form.Position=unifrnd(Param.lb,Param.ub,1,Param.nPlayer);
    Team(n1+n2+i).Form.Knowledge=unifrnd(Param.lb,Param.ub,1,Param.nPlayer);
    Team(n1+n2+i).Subsititude.Position=unifrnd(Param.lb,Param.ub,1,Param.nPlayer);
    Team(n1+n2+i).Subsititude.Knowledge=unifrnd(Param.lb,Param.ub,1,Param.nPlayer);
    Team(n1+n2+i).Cost=MyCost(Team(n1+n2+i).Form.Position,Param.fobj);
end
[~,so]=sort([Team.Cost]);
Team=Team(so);
if BestTeam.Cost>Team(1).Cost
   BestTeam=Team(1);
else
    Team(1)=BestTeam;
end

for i=1:Param.Leaguesize
    if Team(i).Cost<TEAM(i).Cost
       TEAM(i)=Team(i); 
    end
end
   
end
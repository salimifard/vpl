 function x=Strategy1_su(x,Param,BestTeam)
n=Param.nPlayer;
nn=randi([1 n]);
    d=randperm(n,nn);
    for i=d
        ii=randi([1 n]);
        a= x.Formation(i);
        b= x.Subsititude(ii);
        
        x.Formation(i)=b;
        x.Subsititude(ii)=a;
        
    end
    x.Cost=Param.CostFunction(x.Formation);

end
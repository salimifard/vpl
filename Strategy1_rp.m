function x=Strategy1_rp(x,Param,BestTeam)
n=Param.nPlayer;
    nn=randi([1 n]);
    %rp operator for Form Properties
    d=randperm(n,nn);
    for i=d
        ii=randi([1 n]);
        a= x.Formation(i);
        b= x.Formation(ii);
        x.Formation(i)= b;
        x.Formation(ii)= a;
    end
    
    x.Cost=Param.CostFunction(x.Formation);
    

end
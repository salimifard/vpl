function  [x,Team,BestTeam,BestCurrent]=KnowledgeTransfer(x,Team,Param,StrR,BestCurrent,BestTeam)
n=numel(x.Form.Position);
nn=ceil(n*StrR);
k=randperm(n,nn);
w=Param.w;
C1=Param.c1;
C2=Param.c2;
for ii=k
x.Form.Knowledge(ii)=( w*x.Form.Knowledge(ii)+C1 * rand() * (BestCurrent.Form.Knowledge(ii)-x.Form.Knowledge(ii)) +....
    rand() * (BestTeam.Form.Knowledge(ii)-x.Form.Knowledge(ii)));
x.Subsititude.Knowledge(ii)=(w*x.Subsititude.Knowledge(ii)+ C1 * rand() * (BestCurrent.Subsititude.Knowledge(ii)-x.Subsititude.Knowledge(ii)) +...
    C2 * rand() * (BestTeam.Subsititude.Knowledge(ii)-x.Subsititude.Knowledge(ii)));
end

x=simplebound(x,Param);
x.Form.Position=x.Form.Position+x.Form.Knowledge;
x.Subsititude.Position=x.Subsititude.Position+x.Subsititude.Knowledge;
x=simplebound(x,Param);
x.Cost=MyCost(x.Form.Position,Param.fobj);
if x.Cost<BestCurrent.Cost 
   BestCurrent=x; 
end
if x.Cost<BestTeam.Cost 
   BestTeam=x; 
end

    
end
